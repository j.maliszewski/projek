﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Text;
using System.Net;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private Baza_wydarzen wydarzenia = new Baza_wydarzen();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            //ViewBag.Message = "Your application description page.";
            Wydarzenie wydarzenie = new Wydarzenie();
            return View(wydarzenie);
        }
        public ActionResult details(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wydarzenie wydarzenie1 = wydarzenia.wydarzenie.Find(id);
            if(wydarzenie1 == null)
            {
                return HttpNotFound();
            }
            return View(wydarzenie1);
        }

        public ActionResult Lista_wydarzeń()
        {
            using (Baza_wydarzen wydarzenia = new Baza_wydarzen())
            {
                return View(wydarzenia.wydarzenie.ToList());
            }
        }
        [HttpPost]
        public ActionResult About(Wydarzenie wydarzenie)
        {
            
            if (!ModelState.IsValid)
            {
                return View("About",wydarzenie);
            }
            else
            {
                ViewBag.Message = "Stworzono wydarzenie " + wydarzenie.nazwa;
                Baza_wydarzen wydarzenia = new Baza_wydarzen();
                wydarzenia.wydarzenie.Add(wydarzenie);
                wydarzenia.SaveChanges();
                return View("Success");
            }
            
        }
    }
}