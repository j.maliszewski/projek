﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Wydarzenie
    {
        [ScaffoldColumn(false)]
        [Key]
        public int id_wydarzenia { get; set; }

        [Display(Name = "Nazwa wydarzenia")]
        [Required(ErrorMessage = "Musisz wprowadzić nazwę")]
        [StringLength(30)]
        public string nazwa { get; set; }

        [Display(Name = "Miasto")]
        [Required(ErrorMessage = "Musisz podać miasto")]
        [StringLength(30)]
        public string miasto { get; set; }

        [Display(Name = "Ilość biletów")]
        [Required(ErrorMessage = "Musisz podać ilość")]
        public int ilość { get; set; }

        [Display(Name = "Data rozpoczęcia")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        //[Required(ErrorMessage = "Musisz podać date")]
        //[DataType(DataType.Date)]
        public DateTime? data { get; set; }

        [Display(Name = "Odpłatność")]
        [Required(ErrorMessage = "Wprowadź informację")]
        public string odpłatność { get; set; } 

        [Display(Name = "Cena biletu")]
        [Required(ErrorMessage = "Podaj cene")]        
        public int cena { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Musisz wprowadzić email kontakowy")]
        public string Email_Kontaktowy { get; set; }

        [Phone]
        [RegularExpression(@"([\+]){0,1}([0-9]{2})?[\-\s]?[-]?([0-9]{3})\-?[-\s]?([0-9]{3})[-\s]\-?([0-9]{3})$",
                            ErrorMessage = "Numer musi być zapisany w formacie 123-123-123")]
        public string Numer_Kontaktowy { get; set; }
        
    }
}
